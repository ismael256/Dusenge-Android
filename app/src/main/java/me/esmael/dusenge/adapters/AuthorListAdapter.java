package me.esmael.dusenge.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.esmael.dusenge.R;
import me.esmael.dusenge.models.api.Author;

/**
 * Created by esmael256 on 6/24/2017.
 */

public class AuthorListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_CHAPTER = 0;
    private static final int TYPE_EMPTY = 1;
    private List<Author> authorList;
    private OnAuthorClickListener clickListener;

    public void setClickListener(OnAuthorClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CHAPTER:
                View todoView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_author, parent, false);
                return new CompanyViewHolder(todoView);
            case TYPE_EMPTY:
                View emptyView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_empty_items, parent, false);
                return new EmptyViewHolder(emptyView);
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (this.getEmptyCount() == 0)
            return TYPE_CHAPTER;
        else
            return TYPE_EMPTY;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CompanyViewHolder) {
            Author company = this.authorList.get(position);
            ((CompanyViewHolder) holder).bind(company);
        }
    }

    @Override
    public int getItemCount() {
        return this.getTodoCount() + this.getEmptyCount();
    }

    private int getTodoCount() {
        return this.authorList != null ? this.authorList.size() : 0;
    }

    private int getEmptyCount() {
        return this.getTodoCount() == 0 ? 1 : 0;
    }

    public interface OnAuthorClickListener {
        void onAuthorClick(Author company);
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View view) {
            super(view);
        }
    }

    public class CompanyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewChapterTitle;
        private TextView textViewChapterDetails;

        private CardView cardView;
        private Author author;

        public CompanyViewHolder(View itemView) {
            super(itemView);

            this.textViewChapterTitle = (TextView) itemView.findViewById(R.id.author_title_textView);
            this.textViewChapterDetails = (TextView) itemView.findViewById(R.id.author_details_textView);
            this.cardView = (CardView) itemView.findViewById(R.id.card_view);

            cardView.setOnClickListener(this);
        }

        public void bind(Author author) {
            this.author = author;

            this.textViewChapterTitle.setText(this.author.getName());
            this.textViewChapterDetails.setText(this.author.getDetails());

        }

        @Override
        public void onClick(View view) {

            AuthorListAdapter.this.clickListener.onAuthorClick(this.author);
        }
    }
}

