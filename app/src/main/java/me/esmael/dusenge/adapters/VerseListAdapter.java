package me.esmael.dusenge.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.esmael.dusenge.R;
import me.esmael.dusenge.models.api.Author;
import me.esmael.dusenge.models.api.Verse;

/**
 * Created by esmael256 on 6/24/2017.
 */

public class VerseListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_CHAPTER = 0;
    private static final int TYPE_EMPTY = 1;
    private List<Verse> verseList;
    private OnVerseClickListener clickListener;

    public void setClickListener(OnVerseClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setVerseList(List<Verse> verseList) {
        this.verseList = verseList;
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CHAPTER:
                View todoView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_verse, parent, false);
                return new CompanyViewHolder(todoView);
            case TYPE_EMPTY:
                View emptyView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_empty_items, parent, false);
                return new EmptyViewHolder(emptyView);
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (this.getEmptyCount() == 0)
            return TYPE_CHAPTER;
        else
            return TYPE_EMPTY;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CompanyViewHolder) {
            Verse company = this.verseList.get(position);
            ((CompanyViewHolder) holder).bind(company);
        }
    }

    @Override
    public int getItemCount() {
        return this.getTodoCount() + this.getEmptyCount();
    }

    private int getTodoCount() {
        return this.verseList != null ? this.verseList.size() : 0;
    }

    private int getEmptyCount() {
        return this.getTodoCount() == 0 ? 1 : 0;
    }

    public interface OnVerseClickListener {
        void onVerseClick(Verse company);
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View view) {
            super(view);
        }
    }

    public class CompanyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewChapterTitle;
        private TextView textViewChapterDetails;

        private Verse author;

        public CompanyViewHolder(View itemView) {
            super(itemView);

            this.textViewChapterTitle = (TextView) itemView.findViewById(R.id.verse_title_textView);
            this.textViewChapterDetails = (TextView) itemView.findViewById(R.id.verse_details_textView);

            textViewChapterTitle.setOnClickListener(this);
        }

        public void bind(Verse author) {
            this.author = author;

            this.textViewChapterTitle.setText(this.author.getTitle());
            this.textViewChapterDetails.setText(this.author.getDetails());

        }

        @Override
        public void onClick(View view) {

            VerseListAdapter.this.clickListener.onVerseClick(this.author);
        }
    }
}

