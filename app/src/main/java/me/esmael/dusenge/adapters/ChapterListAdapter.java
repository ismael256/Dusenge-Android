package me.esmael.dusenge.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import me.esmael.dusenge.R;
import me.esmael.dusenge.models.api.Chapter;
import me.esmael.dusenge.utils.UtilityMethods;

/**
 * Created by banada ismael on 5/19/2017.
 */

public class ChapterListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_CHAPTER = 0;
    private static final int TYPE_EMPTY = 1;
    private List<Chapter> chapterList;
    private OnChapterClickListener clickListener;

    public void setClickListener(OnChapterClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setChapterList(List<Chapter> chapterList) {
        this.chapterList = chapterList;
        this.notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CHAPTER:
                View todoView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chapter, parent, false);
                return new CompanyViewHolder(todoView);
            case TYPE_EMPTY:
                View emptyView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_empty_items, parent, false);
                return new EmptyViewHolder(emptyView);
            default:
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (this.getEmptyCount() == 0)
            return TYPE_CHAPTER;
        else
            return TYPE_EMPTY;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CompanyViewHolder) {
            Chapter company = this.chapterList.get(position);
            ((CompanyViewHolder) holder).bind(company);
        }
    }

    @Override
    public int getItemCount() {
        return this.getTodoCount() + this.getEmptyCount();
    }

    private int getTodoCount() {
        return this.chapterList != null ? this.chapterList.size() : 0;
    }

    private int getEmptyCount() {
        return this.getTodoCount() == 0 ? 1 : 0;
    }

    public interface OnChapterClickListener {
        void onChapterClick(Chapter company);
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View view) {
            super(view);
        }
    }

    public class CompanyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewChapterTitle;
        private TextView textViewChapterDetails;
        private ImageView imageViewFavourites;

        private CardView cardView;
        private Chapter chapter;

        public CompanyViewHolder(View itemView) {
            super(itemView);

            this.textViewChapterTitle = (TextView) itemView.findViewById(R.id.chapter_title_textView);
            this.textViewChapterDetails = (TextView) itemView.findViewById(R.id.chapter_details_textView);
            this.imageViewFavourites=(ImageView) itemView.findViewById(R.id.imageView_favourites);

            imageViewFavourites.setOnClickListener(v -> {
                imageViewFavourites.setImageResource(R.drawable.ic_favorite_black_24dp);
                UtilityMethods utilityMethods= new UtilityMethods(itemView.getContext());

                if(utilityMethods.getFavorites1()!=null)
                {
                    utilityMethods.AddFavourites1(chapterList.get(getAdapterPosition()));
                }
                else if(utilityMethods.getFavorites2()!=null)
                {
                    utilityMethods.AddFavourites2(chapterList.get(getAdapterPosition()));
                }
                else if(utilityMethods.getFavorites3()!=null)
                {
                    utilityMethods.AddFavourites3(chapterList.get(getAdapterPosition()));
                }
                else if(utilityMethods.getFavorites4()!=null)
                {
                    utilityMethods.AddFavourites4(chapterList.get(getAdapterPosition()));
                }
                else if(utilityMethods.getFavorites5()!=null)
                {
                    utilityMethods.AddFavourites5(chapterList.get(getAdapterPosition()));
                }else
                    {
                        utilityMethods.AddFavourites5(chapterList.get(getAdapterPosition()));
                    }
            });
            this.cardView = (CardView) itemView.findViewById(R.id.card_view);

            cardView.setOnClickListener(this);
        }

        public void bind(Chapter chapter) {
            this.chapter = chapter;

            this.textViewChapterTitle.setText(this.chapter.getTitle());
            this.textViewChapterDetails.setText(this.chapter.getDetails());

        }

        @Override
        public void onClick(View view) {

            ChapterListAdapter.this.clickListener.onChapterClick(this.chapter);
        }
    }
}
