package me.esmael.dusenge.api;

import java.util.List;

import me.esmael.dusenge.models.api.Author;
import me.esmael.dusenge.models.api.Chapter;
import me.esmael.dusenge.models.api.Verse;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by banada ismael on 5/16/2017.
 */

public interface DusengeApi {

    @GET("chapter")
    Observable<List<Chapter>> getChapters();

    @GET("author")
    Observable<List<Author>> getAuthors();

    @GET("chapter_verse/{chapter_id}")
    Observable<List<Verse>> getVerseById(
            @Path("chapter_id") String chapter_id);



    @GET("chapter_author/{author_id}")
    Observable<List<Chapter>> getChapterByAuthorId(
            @Path("author_id") String author_id);


}
