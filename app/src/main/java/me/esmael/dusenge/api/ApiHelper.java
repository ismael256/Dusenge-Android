package me.esmael.dusenge.api;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import me.esmael.dusenge.BuildConfig;
import me.esmael.dusenge.models.api.Author;
import me.esmael.dusenge.models.api.Chapter;
import me.esmael.dusenge.models.api.Verse;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by banada ismael on 5/16/2017.
 */

public class ApiHelper {
    private static DusengeApi service;

    public static DusengeApi getService() {

        if (service == null) {
            Interceptor logInterceptor = new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(logInterceptor)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.HOST)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();

            service = retrofit.create(DusengeApi.class);
        }

        return service;

    }


    public static Observable<List<Chapter>> getChapterData(Context context) {
        return ApiHelper.getService().getChapters()
                .flatMap(chapterList ->
                {
                    if (chapterList != null) {
                        return Observable.just(chapterList);
                    } else {
                        String error = "something went wrong";
                        return Observable.error(new IOException(error));
                    }
                })
                .doOnNext(chapterList ->
                {

                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public static Observable<List<Author>> getAuthorData(Context context) {
        return ApiHelper.getService().getAuthors()
                .flatMap(authorList ->
                {
                    if (authorList != null) {
                        return Observable.just(authorList);
                    } else {
                        String error = "something went wrong";
                        return Observable.error(new IOException(error));
                    }
                })
                .doOnNext(chapterList ->
                {

                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }



    public static Observable<List<Verse>> getChapterDataById(Context context,String chapter_id) {
        return ApiHelper.getService().getVerseById(chapter_id)
                .flatMap(verseList ->
                {
                    if (verseList != null) {
                        return Observable.just(verseList);
                    } else {
                        String error = "something went wrong";
                        return Observable.error(new IOException(error));
                    }
                })
                .doOnNext(verseList ->
                {

                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }




    public static Observable<List<Chapter>> getChaptersByAuthorId(Context context,String author_id) {
        return ApiHelper.getService().getChapterByAuthorId(author_id)
                .flatMap(chapterList ->
                {
                    if (chapterList != null) {
                        return Observable.just(chapterList);
                    } else {
                        String error = "something went wrong";
                        return Observable.error(new IOException(error));
                    }
                })
                .doOnNext(chapterList ->
                {

                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
