package me.esmael.dusenge.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import me.esmael.dusenge.models.api.Chapter;

/**
 * Created by esmael256 on 7/15/2017.
 */

public class UtilityMethods {
    private Context context;
    private SharedPreferences mPrefs;
    private Gson gson;
    public UtilityMethods(Context context)
    {
        this.context= context;
        this.mPrefs= context.getSharedPreferences("Favourites",context.MODE_PRIVATE);
        this.gson= new Gson();

    }


    public void AddFavourites1(Chapter chapter)
    {
        Log.d("chapterto Add",chapter.toString());
        SharedPreferences.Editor editor= mPrefs.edit();
      String chapterString= gson.toJson(chapter);
        editor.putString("Favorite1",chapterString);
        editor.commit();

    }


    public Chapter getFavorites1()
    {
        String jsonChapter=mPrefs.getString("Favorite1","").toString();
     return  gson.fromJson(jsonChapter,Chapter.class) ;

    }

    public void AddFavourites2(Chapter chapter)
    {
        SharedPreferences.Editor editor= mPrefs.edit();
        String chapterString= gson.toJson(chapter);
        editor.putString("favorite2",chapterString);
        editor.commit();

    }


    public Chapter getFavorites2()
    {
        String jsonChapter=mPrefs.getString("favorites2","").toString();
        return  gson.fromJson(jsonChapter,Chapter.class) ;

    }

    public void AddFavourites3(Chapter chapter)
    {
        SharedPreferences.Editor editor= mPrefs.edit();
        String chapterString= gson.toJson(chapter);
        editor.putString("favorite3",chapterString);
        editor.commit();

    }


    public Chapter getFavorites3()
    {
        String jsonChapter=mPrefs.getString("favorites3","").toString();
        return  gson.fromJson(jsonChapter,Chapter.class) ;

    }

    public void AddFavourites4(Chapter chapter)
    {
        SharedPreferences.Editor editor= mPrefs.edit();
        String chapterString= gson.toJson(chapter);
        editor.putString("favorite4",chapterString);
        editor.commit();

    }


    public Chapter getFavorites4()
    {
        String jsonChapter=mPrefs.getString("favorites4","").toString();
        return  gson.fromJson(jsonChapter,Chapter.class) ;

    }

    public void AddFavourites5(Chapter chapter)
    {
        SharedPreferences.Editor editor= mPrefs.edit();
        String chapterString= gson.toJson(chapter);
        editor.putString("favorite2",chapterString);
        editor.commit();

    }


    public Chapter getFavorites5()
    {
        String jsonChapter=mPrefs.getString("favorites5","").toString();
        return  gson.fromJson(jsonChapter,Chapter.class) ;

    }


}
