package me.esmael.dusenge.fragments;

/**
 * Created by esmael256 on 4/13/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import me.esmael.dusenge.R;
import me.esmael.dusenge.activities.VerseActivity;
import me.esmael.dusenge.models.api.Chapter;
import me.esmael.dusenge.utils.UtilityMethods;


public class FavouritesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    TextView fav1title,fav1det;
    TextView fav2title,fav2det;
    TextView fav3title,fav3det;
    TextView fav4title,fav4det;
    TextView fav5title,fav5det;


    public FavouritesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrdersFragment.
     */
    public static FavouritesFragment newInstance(String param1, String param2) {
        FavouritesFragment fragment = new FavouritesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_favourites, container, false);
        UtilityMethods utilityMethods= new UtilityMethods(this.getContext());
        fav1title=  (TextView) rootView.findViewById(R.id.chapter_title_textView_fav1);
        fav1det=  (TextView) rootView.findViewById(R.id.chapter_details_textViewfav1);

        fav2title=  (TextView) rootView.findViewById(R.id.chapter_title_textView_fav2);
        fav2det=  (TextView) rootView.findViewById(R.id.chapter_details_textViewfav2);

        fav3title=  (TextView) rootView.findViewById(R.id.chapter_title_textView_fav3);
        fav3det=  (TextView) rootView.findViewById(R.id.chapter_details_textViewfav3);

        fav4title=  (TextView) rootView.findViewById(R.id.chapter_title_textView_fav4);
        fav4det=  (TextView) rootView.findViewById(R.id.chapter_details_textViewfav4);

        fav5title=  (TextView) rootView.findViewById(R.id.chapter_title_textView_fav5);
        fav5det=  (TextView) rootView.findViewById(R.id.chapter_details_textViewfav5);

        Chapter chapter1= utilityMethods.getFavorites1();
        Chapter chapter2= utilityMethods.getFavorites2();
        Chapter chapter3= utilityMethods.getFavorites3();
        Chapter chapter4= utilityMethods.getFavorites4();
        Chapter chapter5= utilityMethods.getFavorites5();

        CardView cardView1= (CardView)rootView.findViewById(R.id.card_view_fav_one);
        CardView cardView2= (CardView)rootView.findViewById(R.id.card_view_fav_two);
        CardView cardView3= (CardView)rootView.findViewById(R.id.card_view_fav_three);
        CardView cardView4= (CardView)rootView.findViewById(R.id.card_view_fav_four);
        CardView cardView5= (CardView)rootView.findViewById(R.id.card_view_fav_five);
        if(chapter1!=null)
        {
            fav1title.setText(chapter1.getTitle());
            fav1det.setText(chapter1.getDetails());
            cardView1.setOnClickListener(v -> {
                Intent i= new Intent(getContext(), VerseActivity.class);
                i.putExtra("chapter_id",chapter1.getId());
                startActivity(i);
            });
        }

        if(chapter2!=null)
        {
            cardView2.setVisibility(View.VISIBLE);
            fav2title.setText(chapter2.getTitle());
            fav2det.setText(chapter2.getDetails());
            cardView2.setOnClickListener(v -> {
                Intent i= new Intent(getContext(), VerseActivity.class);
                i.putExtra("chapter_id",chapter2.getId());
                startActivity(i);
            });
        }

        if(chapter3!=null)
        {
            cardView3.setVisibility(View.VISIBLE);
            fav3title.setText(chapter3.getTitle());
            fav3det.setText(chapter3.getDetails());
            cardView3.setOnClickListener(v -> {
                Intent i= new Intent(getContext(), VerseActivity.class);
                i.putExtra("chapter_id",chapter3.getId());
                startActivity(i);
            });
        }

        if(chapter4!=null)
        {
            cardView4.setVisibility(View.VISIBLE);
            fav4title.setText(chapter4.getTitle());
            fav4det.setText(chapter4.getDetails());
            cardView4.setOnClickListener(v -> {
                Intent i= new Intent(getContext(), VerseActivity.class);
                i.putExtra("chapter_id",chapter4.getId());
                startActivity(i);
            });
        }

        if(chapter5!=null)
        {
            cardView5.setVisibility(View.VISIBLE);
            fav5title.setText(chapter5.getTitle());
            fav5det.setText(chapter5.getDetails());
            cardView5.setOnClickListener(v -> {
                Intent i= new Intent(getContext(), VerseActivity.class);
                i.putExtra("chapter_id",chapter5.getId());
                startActivity(i);
            });
        }






        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

