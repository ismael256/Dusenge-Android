package me.esmael.dusenge.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import me.esmael.dusenge.R;
import me.esmael.dusenge.activities.VerseActivity;
import me.esmael.dusenge.adapters.ChapterListAdapter;
import me.esmael.dusenge.api.ApiHelper;
import me.esmael.dusenge.models.api.Chapter;
import rx.Observer;

/**
 * Created by banada ismael on 5/19/2017.
 */

public class ChaptersFragment extends Fragment implements ChapterListAdapter.OnChapterClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ChapterListAdapter chapterListAdapter;

    private OnFragmentInteractionListener mListener;
    private MaterialDialog dialogProgress;

    public ChaptersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrdersFragment.
     */
    public static ChaptersFragment newInstance(String param1, String param2) {
        ChaptersFragment fragment = new ChaptersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_chapter, container, false);
        RecyclerView recyclerViewDataList = (RecyclerView) rootView.findViewById(R.id.chapter_list_recyclerView);
        chapterListAdapter= new ChapterListAdapter();
        chapterListAdapter.setClickListener(this);

        this.showProgressDialog(getContext());

        ApiHelper.getChapterData(getContext()).subscribe(new Observer<List<Chapter>>() {
            @Override
            public void onCompleted() {

                ChaptersFragment.this.dialogProgress.dismiss();

                Log.d("onCompleted", "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                ChaptersFragment.this.dialogProgress.dismiss();
                ChaptersFragment.this.showLoginErrorDialog(getContext());
                Log.d("onError", e.toString());

            }

            @Override
            public void onNext(List<Chapter> chapterList) {
                chapterListAdapter.setChapterList(chapterList);
                recyclerViewDataList.setAdapter(chapterListAdapter);
            }
        });



        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onChapterClick(Chapter company) {
        Intent i= new Intent(getContext(), VerseActivity.class);
        i.putExtra("chapter_id",company.getId());
        startActivity(i);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void showIncompleteDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.incomplete_data)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showLoginErrorDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.no_chapters_found)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showProgressDialog(Context context)
    {
        this.dialogProgress = new MaterialDialog.Builder(context)
                .content(R.string.loading_chapters)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }
}
