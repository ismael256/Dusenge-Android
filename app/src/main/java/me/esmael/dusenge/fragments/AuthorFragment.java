package me.esmael.dusenge.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import me.esmael.dusenge.R;
import me.esmael.dusenge.activities.ChapterActivity;
import me.esmael.dusenge.activities.VerseActivity;
import me.esmael.dusenge.adapters.AuthorListAdapter;
import me.esmael.dusenge.api.ApiHelper;
import me.esmael.dusenge.models.api.Author;
import me.esmael.dusenge.models.api.Chapter;
import rx.Observer;

/**
 * Created by esmael256 on 6/24/2017.
 */

public class AuthorFragment extends Fragment implements AuthorListAdapter.OnAuthorClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private AuthorListAdapter chapterListAdapter;

    private OnFragmentInteractionListener mListener;
    private MaterialDialog dialogProgress;
    private  List<Author> authors;

    public AuthorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrdersFragment.
     */
    public static AuthorFragment newInstance(String param1, String param2) {
        AuthorFragment fragment = new AuthorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_authors, container, false);
        RecyclerView recyclerViewDataList = (RecyclerView) rootView.findViewById(R.id.author_list_recyclerView);
        chapterListAdapter= new AuthorListAdapter();
        chapterListAdapter.setClickListener(this);
        authors= new ArrayList<>();

        this.showProgressDialog(getContext());

        ApiHelper.getAuthorData(getContext()).subscribe(new Observer<List<Author>>() {
            @Override
            public void onCompleted() {

                AuthorFragment.this.dialogProgress.dismiss();

                Log.d("onCompleted author", "onCompleted author");
            }

            @Override
            public void onError(Throwable e) {
                AuthorFragment.this.dialogProgress.dismiss();
                AuthorFragment.this.showLoginErrorDialog(getContext());
                Log.d("onError", e.toString());

            }

            @Override
            public void onNext(List<Author> chapterList) {
                authors=chapterList;
                chapterListAdapter.setAuthorList(chapterList);
                recyclerViewDataList.setAdapter(chapterListAdapter);
            }
        });



        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAuthorClick(Author company) {
        Intent i= new Intent(getContext(), ChapterActivity.class);
        i.putExtra("author_id",company.getId());
        startActivity(i);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void showIncompleteDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.incomplete_data)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showLoginErrorDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.no_authors_found)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showProgressDialog(Context context)
    {
        this.dialogProgress = new MaterialDialog.Builder(context)
                .content(R.string.loading_authors)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        //inflater.inflate(R.menu.main,menu);
        super.onCreateOptionsMenu(menu,inflater);


    }


}
