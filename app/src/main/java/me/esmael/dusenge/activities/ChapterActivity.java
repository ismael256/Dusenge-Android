package me.esmael.dusenge.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import me.esmael.dusenge.R;
import me.esmael.dusenge.adapters.ChapterListAdapter;
import me.esmael.dusenge.adapters.VerseListAdapter;
import me.esmael.dusenge.api.ApiHelper;
import me.esmael.dusenge.models.api.Chapter;
import me.esmael.dusenge.models.api.Verse;
import rx.Observer;

/**
 * Created by esmael256 on 7/15/2017.
 */

public class ChapterActivity extends AppCompatActivity implements ChapterListAdapter.OnChapterClickListener{

    private MaterialDialog dialogProgress;
    private ChapterListAdapter chapterListAdapter;
    private List<Chapter> chapters;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Chapters");
        chapters= new ArrayList<>();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Thanks for using this app", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());


        RecyclerView recyclerViewDataList = (RecyclerView) findViewById(R.id.chapter_list_recyclerView);
        chapterListAdapter= new ChapterListAdapter();

        this.showProgressDialog(this);
        chapterListAdapter.setClickListener(this);


        String authorId=getIntent().getStringExtra("author_id");
        ApiHelper.getChaptersByAuthorId(ChapterActivity.this,authorId).subscribe(new Observer<List<Chapter>>() {
            @Override
            public void onCompleted() {

                ChapterActivity.this.dialogProgress.dismiss();

                Log.d("onCompleted", "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                ChapterActivity.this.dialogProgress.dismiss();
                ChapterActivity.this.showLoginErrorDialog(ChapterActivity.this);
                Log.d("onError", e.toString());

            }

            @Override
            public void onNext(List<Chapter> chapterList) {
                chapters=chapterList;
                chapterListAdapter.setChapterList(chapterList);
                recyclerViewDataList.setAdapter(chapterListAdapter);
            }
        });
    }

    private void showIncompleteDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.incomplete_data)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showLoginErrorDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.no_chapters_found)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showProgressDialog(Context context)
    {
        this.dialogProgress = new MaterialDialog.Builder(context)
                .content(R.string.loading_chapters)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;


        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main,menu);
        final MenuItem searchItem= menu.findItem(R.id.action_search);
        if(searchItem!=null){
            SearchView  searchView= (SearchView) android.support.v4.view.MenuItemCompat.getActionView(searchItem);
            searchView.setOnCloseListener(() -> false);
            searchView.setOnSearchClickListener(v -> {

            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    filter(query);
                    Toast.makeText(ChapterActivity.this, "Searching for ... "+query, Toast.LENGTH_SHORT).show();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    filter(newText);
                    Toast.makeText(ChapterActivity.this, "Searching for ... "+newText, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });
        }
        return true;
    }



    void filter(String searchText)
    {
        List<Chapter> temp= new ArrayList<>();
        for(Chapter chapter:this.chapters)
        {
         if(chapter.getTitle().contains(searchText))
         {
          temp.add(chapter) ;
         }
        }
        chapterListAdapter.setChapterList(temp);
        chapterListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onChapterClick(Chapter company) {
        Intent i= new Intent(ChapterActivity.this, VerseActivity.class);
        i.putExtra("chapter_id",company.getId());
        startActivity(i);
    }
}
