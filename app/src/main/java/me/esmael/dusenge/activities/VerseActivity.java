package me.esmael.dusenge.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import me.esmael.dusenge.R;
import me.esmael.dusenge.adapters.VerseListAdapter;
import me.esmael.dusenge.api.ApiHelper;
import me.esmael.dusenge.models.api.Chapter;
import me.esmael.dusenge.models.api.Verse;
import rx.Observer;

public class VerseActivity extends AppCompatActivity implements VerseListAdapter.OnVerseClickListener{

    private MaterialDialog dialogProgress;
    private VerseListAdapter verseListAdapter;
    private List<Verse> verses;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Verses");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Thanks for using this app", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());


        RecyclerView recyclerViewDataList = (RecyclerView) findViewById(R.id.verse_list_recyclerView);
        verseListAdapter= new VerseListAdapter();
        verseListAdapter.setClickListener(this);
        verses= new ArrayList<>();


        this.showProgressDialog(this);


        String chapterId=getIntent().getStringExtra("chapter_id");
        ApiHelper.getChapterDataById(VerseActivity.this,chapterId).subscribe(new Observer<List<Verse>>() {
            @Override
            public void onCompleted() {

                VerseActivity.this.dialogProgress.dismiss();

                Log.d("onCompleted", "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                VerseActivity.this.dialogProgress.dismiss();
                VerseActivity.this.showLoginErrorDialog(VerseActivity.this);
                Log.d("onError", e.toString());

            }

            @Override
            public void onNext(List<Verse> chapterList) {
                verses=chapterList;
                verseListAdapter.setVerseList(chapterList);
                recyclerViewDataList.setAdapter(verseListAdapter);
            }
        });
    }

    private void showIncompleteDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.incomplete_data)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showLoginErrorDialog(Context context)
    {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_error)
                .content(R.string.no_chapters_found)
                .positiveText(R.string.dialog_positive)
                .show();
    }

    private void showProgressDialog(Context context)
    {
        this.dialogProgress = new MaterialDialog.Builder(context)
                .content(R.string.verse_loading)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    @Override
    public void onVerseClick(Verse company) {


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;


        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main,menu);
        final MenuItem searchItem= menu.findItem(R.id.action_search);
        if(searchItem!=null){
            SearchView searchView= (SearchView) android.support.v4.view.MenuItemCompat.getActionView(searchItem);
            searchView.setOnCloseListener(() -> false);
            searchView.setOnSearchClickListener(v -> {

            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    filter(query);
                    Toast.makeText(VerseActivity.this, "Searching for ...  "+query, Toast.LENGTH_SHORT).show();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    filter(newText);
                    Toast.makeText(VerseActivity.this, "Searching for ...  " +newText, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });
        }
        return true;
    }



    void filter(String searchText)
    {
        List<Verse> temp= new ArrayList<>();
        for(Verse chapter:verses)
        {
            if(chapter.getTitle().contains(searchText))
            {
                temp.add(chapter) ;
            }
        }
        verseListAdapter.setVerseList(temp);
        verseListAdapter.notifyDataSetChanged();
    }
}
