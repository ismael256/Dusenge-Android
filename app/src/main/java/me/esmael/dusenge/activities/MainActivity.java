package me.esmael.dusenge.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import me.esmael.dusenge.R;
import me.esmael.dusenge.fragments.AuthorFragment;
import me.esmael.dusenge.fragments.ChaptersFragment;
import me.esmael.dusenge.fragments.FavouritesFragment;
import me.esmael.dusenge.fragments.HelpFragment;
import me.esmael.dusenge.fragments.SettingsFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ChaptersFragment.OnFragmentInteractionListener,AuthorFragment.OnFragmentInteractionListener,
HelpFragment.OnFragmentInteractionListener,FavouritesFragment.OnFragmentInteractionListener,SettingsFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view -> Snackbar.make(view, "Thanks for using Dusenge App", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        MainActivity.this.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frameLayout, AuthorFragment.newInstance(null,null))
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment=null;

        switch (id)
        {
            case R.id.nav_chapters:
               fragment= ChaptersFragment.newInstance(null,null);

                break;

            case R.id.nav_authors:
                fragment= AuthorFragment.newInstance(null,null);
                break;
            case R.id.nav_favorites:
                fragment= FavouritesFragment.newInstance(null,null);
                break;

            case R.id.nav_help:
                 fragment= HelpFragment.newInstance(null,null);
                break;

            case R.id.nav_settings:
                 fragment= SettingsFragment.newInstance(null,null);
                break;



        }



        if(fragment!=null)
        {
            MainActivity.this.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frameLayout, fragment)
                    .commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
